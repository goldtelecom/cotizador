<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

Auth::routes();

Route::get('/api', 'HomeController@api');

Route::post('/search_key/{type}', 'HomeController@search_key');

Route::post('/generate_pdf', 'HomeController@generate_pdf');

Route::post('/search', ['as' => 'search','uses'=>'HomeController@search']);
Route::post('/search_lat', ['as' => 'search_lat','uses'=>'HomeController@search_by_coor']);
Route::get('/latitude/{type}', ['as' => 'latitude','uses'=>'HomeController@latitude']);

Route::get('/', 'HomeController@index')->name('home');

Route::get('/search_history', 'HomeController@history');

Route::get('/quote/{id}', 'HomeController@quote');

Route::get('/search/{type?}', 'HomeController@index');

Route::resource('users', 'UserController');

Route::resource('roles', 'RoleController');

Route::resource('permissions', 'PermissionController');

Route::resource('posts', 'PostController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

