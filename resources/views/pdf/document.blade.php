<html>
<head>
	<style>
		@page {
			header: page-header;
			footer: page-footer;
		}

		hr {
			padding: 100;
		}
	</style>
</head>
<body>
<table  cellpadding="5">
	<tr>
		<td align="right" colspan=4  height="80" valign="top">
			<img src="/img/logo.png" class="mt-2" alt="">
		</td>
	</tr>
	<tr>
		<td align="center" colspan=4>
			<H2>SERVICE QUOTE</H2>
			<HR>
		</td>
	</tr>
	<tr>
		<td colspan=2>
			Quote Issue Date: {{ date('m/d/Y') }} <br>                                                                            
			Customer Name: {{ $company -> title }}
		</td>
		<td colspan=2>
			Quote Number: 
		</td>
	</tr>
	<tr>
		<td colspan=4>
			<hr>
			<B>TECHNICAL SPECIFICATIONS</B>
			<P>
		</td>
	</tr>
	<tr>
		<td colspan=4>
			@if ($request -> product == 'ethernet')
				Site A: Golddata NNI<BR>
			@endif
			Site {{ $request -> product === "dia" ? "A" : "Z" }} : {{ $result['body'][0]['locations'][0]['building_name'] }}, {{ $result['body'][0]['locations'][0]['address_information']['street_address'] }}, {{ $result['body'][0]['locations'][0]['address_information']['city'] }}, {{ $result['body'][0]['locations'][0]['address_information']['country'] }}
		</td>
	</tr>
	<tr>
		<td colspan=4>
			Product: {{ $request -> product === "dia" ? "Direct Internet Access" : "Ethernet Layer 2 Solution" }} <BR>
			CPE: Yes or No <BR>
			Cross Connections: 
		</td>
	</TR>
	<tr>
		<td colspan=4>
			<HR>
			<B>PRICING (US Dollars) AND SERVICE TERM</B>
		</td>
	</tr>
	<tr>
		<td>
			Term: 12 months 
		</td>
		<td> 
		    Delivery Time:
		</td>
		<td>                                                
			MRC: US$ {{ number_format($mrc_12,2) }}
		</td>
		<td>
			NRC: US$ {{ number_format($nrc_12,2) }}
		</td>
	</tr>
	<tr>
		<td>
			Term: 24 months 
		</td>
		<td> 
		    Delivery Time:
		</td>
		<td>                                                
			MRC: US$ {{ number_format($mrc_24,2) }}
		</td>
		<td>
			NRC: US$ {{ number_format($nrc_24,2) }}
		</td>
	</tr>
	<tr>
		<td>
			Term: 36 months 
		</td>
		<td> 
		    Delivery Time:
		</td>
		<td>                                                
			MRC: US$ {{ number_format($mrc_36,2) }}
		</td>
		<td>
			NRC: US$ {{ number_format($nrc_36,2) }}
		</td>
	</tr>
	<tr>
		<td colspan=4>
			<hr>
				<B>CONTACT INFORMATION</B>
			</hr>
		</td>
	</tr>
	<tr>
		<td colspan=4>
			Commercial Contact:  {{ $id -> name }} <BR>
			Email:  {{ $id -> email }}                 
		</td>
	</tr>
	<tr>
		<td colspan=4>
			<hr>
			<B>ADDITIONAL TERM AND CONDITIONS</B>
		</td>
	</tr>
	<tr>
		<td colspan=4>
			- Prices in US Dollars without Taxes or Cross Connections <BR>
			- Not Latency Guarantee associated with this service<BR>
			- Quote Valid for 30 days<BR>
			- CPE Pricing not included. <BR>
			- When Near-Net or Limited Access, prices are subject to feasibility analysis or Site Survey<BR>
		</td>
	</tr>
	<tr>
		<td colspan=4>
			<hr>
			<B>CUSTOMER ACCEPTANCE</B>
		</td>
	</tr>
	<tr>
		<td height="120" valign="bottom">
			__________________________   <br> 
			<center>Customer Signature.</center>
		</td>
		<td height="120" valign="bottom">    
			____________________ <br>
			<center>Customer Name</center>
		</td>
		<td height="120" valign="bottom">
		    ________________ <br>
		    <center>Title</center>
		 </td>
		 <td height="120" valign="bottom">
		   	_________      <br>
		   	<center>Date</center>
		</td>
	</tr>
</table>
<htmlpagefooter name="page-footer">
	<center>
		Document property of Gold Telecom Inc.<br>
		All rights reserved
	</center>
</htmlpagefooter>

</body>
</html>