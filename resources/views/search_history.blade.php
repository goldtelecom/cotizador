@extends('layouts.base')

@section('content')
<!-- Page Content -->
<div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light border-bottom">
        <button class="btn btn-bars" id="menu-toggle"><i class="fas fa-bars"></i></button>

        <div class="navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
              <a class="nav-link" href="#"> {{ Auth::user()->name }} </a>
            </li>
          </ul>
        </div>
      </nav>

      <div class="container-fluid">
        <h1 class="mt-4 mb-3 title">Search History </h1>
                <table id="example" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Building Name</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Country</th>
                            <th>Customer</th>                            
                            <th>Term</th>
                            <th>Bandwidth</th>
                            <th>Product</th>
                            <th>MRC</th>
                            <th>NRC</th>
                            <th>Note</th>
                            <th>Date</th>
                            <th>User</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($history as $record)
                        <tr>
                            <td>{{ $record -> id }}</td>
                            <td><a href="/quote/{{ $record -> id }}">
                                @if (isset($record -> building_name))
                                    {{ $record -> building_name }}
                                @else
                                    {{ $record -> address }}
                                @endif
                            </a></td>
                            <td>{{ $record -> address }}</td>
                            <td>{{ $record -> city }}</td>
                            <td>{{ $record -> state }}</td>
                            <td>{{ $record -> country }}</td>
                            <td>{{ $record -> customer_name }}</td>
                            <td>{{ $record -> term }}</td>
                            <td>{{ str_replace('M',' Mbps',substr($record -> bandwidth,9)) }}</td>
                            <td>{{ $record -> product === "dia" ? "Direct Internet Access" : "Ethernet Layer 2 Solution" }}</td>
                            <td>{{ number_format($record -> mrc,2) }}</td>
                            <td>{{ number_format($record -> nrc,2) }}</td>
                            <td>{{ $record -> notes }}</td>
                            <td>{{ $record -> created_at }}</td>
                            <td>{{ $record -> company -> name }}</td>
                        @endforeach
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
@endsection