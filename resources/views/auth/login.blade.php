<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Login | Gold Data</title>
  <link rel="icon" href="http://golddata.net/wp-content/uploads/2018/03/favicon.png">

  <!-- Bootstrap core CSS -->
  <link href="{{ asset('css/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- Custom styles for this template-->
  <link href="{{ asset('css/simple-sidebar.css') }}" rel="stylesheet">
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">

</head>

<body class="bg-golddata wlogin">

  <div class="overlay"></div>
  <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
    <source src="videos/v2.mp4" type="video/mp4">
  </video>


  <div class="container">
    <div class="row">

      <div class="col-lg-12 col-12 mt-100">
        <br>
        <img src="img/logo-sticky-light-golddata.png" style="display: block;" alt="">
        <br><br>
      </div>

      <div class="col col-lg-7">
        <span class="text-white h3">Gold Data's pricing tool</span>
        <br>
        <h1>
          <a href="" class="typewrite" data-period="2000" data-type='[ "A DYNAMIC", "REAL-TIME", "PRICING ENGINE"]'>
            <span class="wrap"></span>
          </a>
        </h1>
        <br> 
        <p class="text-white">Do you want to know what happens when you make a request?</p>
        <ul class="text-white">
          <li>Our platform analyzes more than 30 Million On-Net and Near-Net buildings in Latin America and The Caribbean.</li>
          <li>It looks through more than 100,000 km of fiber optic connections back to the USA, to find the best route.</li>
          <li>Then, consolidates pricing data for more than 11 Sub-sea Cable Systems, interconnecting the region, and brings accurate pricing data for more than 13 countries and The Caribbean islands.</li>
        </ul>

        <span class="text-white h6">This is the new level of automation, this is Gold Data</span>
      </div>

      <div class="col-1"></div>
      <div class="col col-lg-4">
        <div class="card shadow dark card-login mx-auto">
          <div class="card-header">Login</div>
          <div class="card-body">
            <form class="mb4" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
              <div class="form-group">
                <div class="form-label-group">
                  <input
                          id="email"
                          type="email"
                          class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                          name="email"
                          value="{{ old('email') }}"
                          required
                          autofocus
                  >
                  @if ($errors->has('email'))
                      <div class="invalid-feedback">
                          <strong>{{ $errors->first('email') }}</strong>
                      </div>
                  @endif
                  <!-- <label for="inputEmail">Email address</label> -->
                </div>
              </div>
              <div class="form-group">
                <div class="form-label-group">
                  <input
                          id="password"
                          type="password"
                          class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                          name="password"
                          required
                  >

                  @if ($errors->has('password'))
                      <div class="invalid-feedback">
                          <strong>{{ $errors->first('password') }}</strong>
                      </div>
                  @endif
                  <!-- <label for="inputPassword">Password</label> -->
                </div>
              </div>
              <div class="form-group">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" value="remember-me">
                    Remember Password
                  </label>
                </div>
              </div>
              <button type="submit" class="btn btn-primary btn-block btn-calculate">Login</button>
            </form>
            <div class="text-center">
              <!-- <a class="d-block small mt-3" href="register.html">Register an Account</a> -->
              <a class="d-block small solicitud" href="mailto:">Request Access</a>
              <br>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="css/vendor/jquery/jquery.min.js"></script>
  <script src="css/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="css/vendor/jquery-easing/jquery.easing.min.js"></script>

  <script>
    var TxtType = function(el, toRotate, period) {
        this.toRotate = toRotate;
        this.el = el;
        this.loopNum = 0;
        this.period = parseInt(period, 10) || 2000;
        this.txt = '';
        this.tick();
        this.isDeleting = false;
    };

    TxtType.prototype.tick = function() {
        var i = this.loopNum % this.toRotate.length;
        var fullTxt = this.toRotate[i];

        if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
        } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
        }

        this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

        var that = this;
        var delta = 200 - Math.random() * 100;

        if (this.isDeleting) { delta /= 2; }

        if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
        } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
        }

        setTimeout(function() {
        that.tick();
        }, delta);
    };

    window.onload = function() {
        var elements = document.getElementsByClassName('typewrite');
        for (var i=0; i<elements.length; i++) {
            var toRotate = elements[i].getAttribute('data-type');
            var period = elements[i].getAttribute('data-period');
            if (toRotate) {
              new TxtType(elements[i], JSON.parse(toRotate), period);
            }
        }
        // INJECT CSS
        // var css = document.createElement("style");
        // css.type = "text/css";
        // css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
        // document.body.appendChild(css);
    };
  </script> 


</body>

</html>
