@extends('layouts.base')

@section('content')
<style>
  .btn-search
    {
        background-color: #1463AB;
        color:#FFF;
        border-color: #2F3E48;
    }

    .btn-search:hover {
        color: #FFCA05;
    }
</style>
<!-- Page Content -->
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light border-bottom">
        <button class="btn btn-bars" id="menu-toggle"><i class="fas fa-bars"></i></button>

        <div class="navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
              <a class="nav-link" href="#"> {{ Auth::user()->name }} </a>
            </li>
          </ul>
        </div>
      </nav>

      <div class="container-fluid">
        <h1 class="mt-4 mb-3 title">Welcome to Gold Data Connect </h1>
        <div class="row mb-2">
          <div class="col-12 col-lg-6">
              <a class="btn-selector dia {{ $dia_active }}" href="/search/dia">Dedicated Internet Access</a>
          </div>
          <div class="col-12 col-lg-6">
              <a class="btn-selector el2s {{ $eth_active }}" href="/search/ethernet">Ethernet Layer 2 Solutions</a>
          </div>
        </div> <!-- row -->
        <div class="row">
          <div class="col-6 col-lg-6">
              <button onclick="location.href = '/search/{{$type}}';" id="myButton" class="btn-search btn" >Seach by Address</button>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <hr>
          </div>
        </div>

        <form action="/search_lat" method="post">
                      {{ csrf_field() }}
            <input type="hidden" name="service_type" value="{{$type}}">
          <div class="row mt-2">
            <div class="col-12">
              <div class="card shadow">
                <div class="card-body">
                  <div class="row">

                    <div class="col-12 col-lg-3">
                      <div class="form-group">
                        <label for="">Requirement Date</label>
                        <input name="req_date" type="text" class="form-control" id="" aria-describedby="" value="{{ date('m/d/Y') }}" readonly>
                      </div>
                    </div>

                    <div class="col-12 col-lg-3">
                      <div class="form-group">
                        <label for="">Customer Name</label>
                        <input name="cust_name" type="text" class="form-control" id="" aria-describedby="" value="{{ $company->title }}" readonly>
                      </div>
                    </div>

                  </div>
                </div>
              </div>  
            </div>
          </div> <!-- row -->
        @if ($type == 'ethernet')
          <div class="row mt-4">
            <div class="col-12">
              <div class="card shadow">
                <div class="card-header">
                  Site A
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-12 col-lg-3">
                      <fieldset disabled>
                        <div class="form-group">
                          <input name="site_a" type="text" id="disabledTextInput" class="form-control" placeholder="Gold Data NNI">
                        </div>
                      </fieldset>
                    </div>

                  </div>
                </div>
              </div>

              <img src="images/line.png" class="mt-2" alt="">
            </div>
          </div> <!-- row -->
        @endif
          <div class="row mt-2">
            <div class="col-12">
              <div class="card shadow">
                <div class="card-header">
                  Site {{ $type === "dia" ? "A" : "Z" }} Info 
                </div>
                <div class="card-body">
                  <div class="row">

                    <div class="col-12 col-lg-3">
                      <div class="form-group">
                        <label for="">Latitude, Longitude</label>
                        {{ Form::text('latitude', null, array('class' => 'form-control')) }}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> <!-- row -->

          <div class="row mt-4">
            <div class="col-12">
              <div class="card shadow">
                <div class="card-header">
                  Service Information
                </div>
                <div class="card-body">
                  <div class="row">

                    <div class="col-12 col-lg-3">
                      <div class="form-group">
                        <label for="">Term</label>
                        <select name="term" class="form-control" id="term" required>
                          
                          <option value="12">12 Months</option>
                          <option value="24">24 Months</option>
                          <option value="36">36 Months</option>
                        </select>
                      </div>
                    </div>
                    
                    <div class="col-12 col-lg-3">
                      <div class="form-group">
                        <label for="">Bandwidth</label>
                        <select name="bandwidth" class="form-control" id="bandwidth" required>
                          
                          @for ($i = 2; $i < 20; $i = $i+2) 
                            <option value="{{ $i }}">{{ $i }} mbps</option>
                          @endfor
                          @for ($i = 2; $i < 11; $i++) 
                            <option value="{{ $i*10 }}">{{ $i*10 }} mbps</option>
                          @endfor
                          <option value="150">150 mbps</option>
                          @for ($i = 200; $i < 1001; $i = $i+100) 
                            <option value="{{ $i }}">{{ $i }} mbps</option>
                          @endfor
                        </select>
                      </div>
                    </div>

                    <div class="col-12 col-lg-3">
                      <div class="form-group">
                        <label for="">Customer Name</label>
                        <input name="customer_name" type="text" class="form-control" id="" aria-describedby="" placeholder="Ex. End customer name">
                      </div>
                    </div>

                    <div class="col-12 col-lg-3">
                      <div class="form-group">
                        <label for="">Notes <i class="fas fa-question-circle" data-toggle="tooltip" title="Technical requirements to consider on the solution."></i></label>
                        <input name="notes" type="text" class="form-control" id="" aria-describedby="" placeholder="Ex. MTU, Jumbo Frame, Interphase, etc.">

                        <!-- Generated markup by the plugin -->
                        <div class="tooltip bs-tooltip-top" role="tooltip">
                          <div class="arrow"></div>
                          <div class="tooltip-inner">
                            Technical requirements to consider on the solution.
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div> <!-- row -->

          <div class="row mt-4 pb-1">
            <div class="col-12">
                <button type="reset" class="btn btn-primary btn-clear">Clear</button> 
                <button type="submit" class="btn btn-primary btn-calculate">Search</button>
            </div>
          </div>

        </form>
        <form action="/search_key/lat" method="post">
            {{ csrf_field() }}
            @if (isset($result_form)) 
            <input type="hidden" name="bandwidth" id="bandwidth" value="{{ $bandwidth }}">
            <input type="hidden" name="term" id="term" value="{{ $term }}">
            <input type="hidden" name="customer_name" id="customer_name" value="{{ $customer_name }}">
            <input type="hidden" name="notes" id="notes" value="{{ $notes }}">
            <input type="hidden" name="service_type" value="{{ $type }}">
            @endif
        <div class="row mt-1 mb-5 results" id="results">
          <div class="col-12">
            <!-- <div class="collapse" id="collapseExample">  -->
              <hr class="mb-4">
              <div class="card shadow">
                <div class="card-header">
                  <div class="row">
                    <div class="col-sm-6">
                      Results
                    
                    @if(Session::has('message'))
                      <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}.
                      </P>
                    @endif
                    </div>
                    @if (isset($result))
                        <div class="col-sm-3">
                          Customer Name
                        </div>
                        <div class="col-sm-3">
                          Notes
                        </div>
                    @endif
                  </div>
                  <br>
                  </form>
                  <form action="/generate_pdf" method="post">
                      {{ csrf_field() }}
                    @if (isset($result['body']))
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="hidden" value="{{ $key }}" name="key">
                                <input type="hidden" value="{{ $lat }}, {{ $long }}" name="coord">
                                <input type="hidden" value="{{ $customer_name }}" name="customer_name">
                                <input type="hidden" value="{{ $notes }}" name="notes">
                                <input type="hidden" value="{{ $term }}" name="term">
                                <input type="hidden" value="{{ $bandwidth }}" name="bandwidth">
                                <input type="hidden" value="{{ $type }}" name="product">
                                <H3>Lat : {{ $lat }} Long : {{ $long }}<BR>{{ $result['body'][0]['locations'][0]['address_information']['country'] }}<BR>Bandwidth : 
                                {{ str_replace('M',' Mbps',substr($bandwidth,9)) }}<BR>Term : {{ $term }} months<BR>Product : {{ $type === "dia" ? "Direct Internet Access" : "Ethernet Layer 2 Solution" }}</H3>
                            </div>
                            <div class="col-sm-3">
                                <h3>{{ $customer_name }}</h3>
                            </div>
                            <div class="col-sm-3">
                                <h3>{{ $notes }}</h3>
                            </div>
                        </div>
                    @elseif (isset($result['error']))
                        {{ $result['error'] }} {{ $key }}
                    @endif
                </div>
                <div class="card-body">
                  <div class="row">

                    <div class="col-lg-10 mb-2">
                      <div class="row">
                        <div class="col-12 col-lg-3">
                          <h3 style="color:black;"><i class="far fa-calendar-alt"></i> Monthy Price</h3>
                          @if (isset($mrc_12)) 
                            <h3>{{ number_format($mrc_12,2) }} <span>USD</span></h3>
                            <h3>{{ number_format($mrc_24,2) }} <span>USD</span></h3>
                            <h3>{{ number_format($mrc_36,2) }} <span>USD</span></h3>
                          @endif
                        </div>

                        <div class="col-12 col-lg-3">
                          <h3 style="color:black;"><i class="far fa-calendar-check"></i> Installing Fee</h3>
                          @if (isset($nrc_12)) 
                            <h3>{{ number_format($nrc_12,2) }} <span>USD</span></h3>
                            <h3>{{ number_format($nrc_24,2) }} <span>USD</span></h3>
                            <h3>{{ number_format($nrc_36,2) }} <span>USD</span></h3>
                          @endif
                        </div>

                        <div class="col-12 col-lg-3">
                          <h3 style="color:black;"><i class="far fa-calendar-check"></i> Contract Length</h3>
                          @if (isset($nrc_12)) 
                            <h3>12 <span>Months</span></h3>
                            <h3>24 <span>Months</span></h3>
                            <h3>36 <span>Months</span></h3>
                          @endif
                        </div>
                      </div>

                    </div>
                    <div class="col-lg-2">
                      <div class="row">
                        <div class="col-12">
                          <button type="submit" class="btn btn-primary btn-calculate float-right mb-4">Quote</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  </div>
                </div>
              </div> <!-- card -->
            <!-- </div> -->
          </div>
        </div> <!-- row -->
        </form>
      </div> <!-- container-fluid -->
    </div>
    <!-- /#page-content-wrapper -->
@endsection