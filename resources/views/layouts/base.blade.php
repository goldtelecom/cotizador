<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Quotes | Gold Data</title>
  <link rel="icon" href="http://productos.golddata.site/assets/img/LogoGoldData.png">

  <!-- Bootstrap core CSS -->
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <!-- Custom styles for this template -->
  <link href="/css/simple-sidebar.css" rel="stylesheet">
  <link rel="stylesheet" href="/css/style.css">
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" class="init">
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
</head>

<body>

  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
      <div class="sidebar-heading"> <img src="/img/NewLogoGD.png" alt="" width="200"> </div>
      <div class="list-group list-group-flush">
        <a href="/" class="list-group-item list-group-item-action bg-light"><i class="fas fa-home "></i> Home</a>
        <a href="/search_history" class="list-group-item list-group-item-action bg-light"><i class="fas fa-angle-double-left mr-1"></i> Previous Quotes</a>
        <a href="#" class="list-group-item list-group-item-action bg-light"><i class="far fa-envelope mr-1"></i> Contact Gold Data</a>
        <a href="#" class="list-group-item list-group-item-action bg-light"><i class="far fa-file mr-2"></i> Quote Reports</a>
        <a href="{{ url('/logout') }}" class="list-group-item list-group-item-action bg-light"><i class="far fa-sign-out-alt mr-2"></i> Logout</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    @yield('content')

  </div>
  <!-- /#wrapper -->
  @yield('script')
  <!-- Bootstrap core JavaScript -->

  

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>

  <script>
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
  </script>

</body>

</html>
