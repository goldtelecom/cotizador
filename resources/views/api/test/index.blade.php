@extends('layouts.app')

@section('title', '| Roles')

@section('content')
<div class="container">
    <div class="row">
        <div class='col-lg-10 col-lg-offset-4'>
            {{ $result['buildingname'] }}<HR>
            {{ $result['street'] }}, {{ $result['city'] }}, {{ $result['state'] }}, {{ $result['country'] }}<HR>
            Latitude : {{ $result['latitude'] }}<BR>
            Longitude : {{ $result['longitude'] }}<HR>
            @foreach($result['serviceSpeedList'] as $var)
                {{ $var['serviceName'] }} - {{ $var['speedName'] }}<P>
                @if (array_key_exists('buildingMrc_12M', $var))
                    12 Meses : US$ {{ number_format($var['buildingMrc_12M'],2) }}<P>
                    24 Meses : US$ {{ number_format($var['buildingMrc_24M'],2) }}<P>
                    36 Meses : US$ {{ number_format($var['buildingMrc_36M'],2) }}
                @else
                    Por favor contáctenos
                @endif
                <hr>
            @endforeach
        </div>
    </div>
</div>

@endsection