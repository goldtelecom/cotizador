<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SearchHistory extends Model
{
    public $table = 'search_history';

    protected $guarded = [];

    public function company() {
        return $this -> hasOne('App\User','id','user_id');
    }

}

