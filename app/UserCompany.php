<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCompany extends Model
{
    public $table = 'user_has_company';

    protected  $primaryKey = 'user_id';

    public function company() {
        return $this -> hasOne('App\Companies','id','id');
    }
}
