<?php

namespace App\Http\Controllers;
//require ‘vendor/autoload.php’;
//use Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Request as GuzzleClient;
use Auth;
use App\UserCompany;
use App\Companies;
use Illuminate\Http\Request;
use PDF;
use App\SearchHistory;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type = 'dia')
    {
        $dia_active = '';
        $eth_active = '';
        if ($type == 'dia') {
            $dia_active = 'active';
            $eth_active = '';
        } elseif ($type == 'ethernet') {
            $dia_active = '';
            $eth_active = 'active';
        }
        $id = Auth::id();
        $userCompany = UserCompany::find($id);
        //dd($userCompany->id);
        $company = Companies::find($userCompany->company_id);
        //dd($company->title);

        return view('index',compact('company','type','dia_active','eth_active'));
    }

    public function latitude($type = 'dia')
    {
        $dia_active = '';
        $eth_active = '';
        if ($type == 'dia') {
            $dia_active = 'active';
            $eth_active = '';
        } elseif ($type == 'ethernet') {
            $dia_active = '';
            $eth_active = 'active';
        }
        $id = Auth::id();
        $userCompany = UserCompany::find($id);
        //dd($userCompany->id);
        $company = Companies::find($userCompany->company_id);
        //dd($company->title);

        return view('index_lat',compact('company','type','dia_active','eth_active'));
    }

    public function search(Request $request)
    {
        try {
            //dd($request);
            $type = $request -> service_type;
            if ($request -> service_type == 'dia') {
                $dia_active = 'active';
                $eth_active = '';
                $service = 'Dedicated Internet';
            } elseif ($request -> service_type == 'ethernet') {
                $dia_active = '';
                $eth_active = 'active';
                $service = 'Ethernet - Switched';
            }
            $term = $request->term;
            $customer_name = $request->customer_name;
            $notes = $request->notes;
            $bandwidth = 'ETHERNET '.$request->bandwidth.'M';
            $id = Auth::id();
            $userCompany = UserCompany::find($id);
            //dd($userCompany->id);
            $company = Companies::find($userCompany->company_id);
            //dd($company->title);
            $address = $request->address;
            $country = $request->country;
            $city = $request->city;
            $client = new Client(['base_uri' => 'https://api.connected2fiber.com']);
            $headers = ['Ocp-Apim-Subscription-Key' => '8f2f532dd5e34f83bfb39747c48450eb'];
            //$headers = ['Ocp-Apim-Subscription-Key' => '8a9e99afc6be4724ac51dc37a7e7644b'];
            //$request = new GuzzleClient('GET', 'https://api.connected2fiber.com/v2/buildings/parsed_address_search?companyId=535&street_name=Calle 4&page=0&size=100',$headers);
            
            $request_api = new GuzzleClient('GET', 'https://api.connected2fiber.com/v2/buildings/parsed_address_search?companyId=535&street_name='.$address.'&country='.$country,$headers);
            //$request_api = new GuzzleClient('GET', 'https://api.connected2fiber.com/availability/v3/companies/535/locations?limit=10&lat=-34.6154692&lon=-58.3831293&radius=2000',$headers);
            $response = $client->send($request_api);
            
            $result_form = json_decode($response->getBody()->getContents(),true);
            
            $bandwidth = 'ETHERNET '.$request->bandwidth.'M';
            //dd("DIA".$dia_active);
            Session::forget('message');  
            return view('index', compact('result_form','company','mrc','nrc','term','bandwidth','customer_name','notes','dia_active','eth_active','type','address'));
        }
        catch (\Exception $e) {
            //dd($e);
            if($e instanceof \GuzzleHttp\Exception\ClientException ){
                Session::flash('message', 'No results for this search! Please search for a different address'); 
                Session::flash('alert-class', 'alert-danger'); 
                if ($city <> '') {
                    $address .= ',+'.$city;
                }
                if ($country <> '') {
                    $address .= ',+'.$country;
                }
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                //dd($response);
                return view('index', compact('result_form','company','mrc','nrc','term','bandwidth','customer_name','notes','dia_active','eth_active','type','address'));
            }
          report($e);
          return false;
        }
    }

    public function search_by_coor(Request $request)
    {
        try {
            //dd('aca');
            $term = $request->term;
            //$radious = $request->radious;
            $customer_name = $request->customer_name;
            $notes = $request->notes;
            $bandwidth = 'ETHERNET '.$request->bandwidth.'M';
            $id = Auth::id();
            $userCompany = UserCompany::find($id);
            //dd($userCompany->id);
            $company = Companies::find($userCompany->company_id);
            //dd($company->title);
            $address = $request->address;
            $country = $request->country;
            $city = $request->city;
            $coord = $request -> latitude;
            $type = $request -> service_type;
            if ($request -> service_type == 'dia') {
                $dia_active = 'active';
                $eth_active = '';
                $service = 'Dedicated Internet';
            } elseif ($request -> service_type == 'ethernet') {
                $dia_active = '';
                $eth_active = 'active';
                $service = 'Ethernet - Switched';
            }
            $lat = strstr($request->latitude,',',true);
            $long = substr(strstr($request->latitude,','),2);
            //dd($long);
            $google_key = 'AIzaSyDxFYl4mx_9Eyg4slqjG-QmAU3PmxCqkJ0';
            $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($long).'&sensor=false&key='.$google_key;

            $json = @file_get_contents($url);
            
            $data=json_decode($json);
            if (!$data) {
                Session::flash('message', 'No results for this search! Please search for a different address'); 
                Session::flash('alert-class', 'alert-danger'); 
                return view('index_lat', compact('result_form','company','mrc','nrc','term','bandwidth','customer_name','notes','dia_active','eth_active','type','address','lat','long'));
            } else {
                $status = $data->status;
            }
            
            $onnet_countries = array('Panama','Venezuela','Costa Rica','Dominican Republic','Brazil');
            if($status=="OK")
            {
                if(in_array(end($data->results)->formatted_address,$onnet_countries)) {
                    $radious = 2000;
                } else {
                    $radious = 200;
                }
                //dd(end($data->results)->formatted_address);
            }
            else
            {
                Session::flash('message', 'Please contact our sales team at access@golddata.net for additional support on this requirement.'); 
                Session::flash('alert-class', 'alert-danger');
            }
            $client = new Client(['base_uri' => 'https://api.connected2fiber.com']);
            $headers = ['Ocp-Apim-Subscription-Key' => '8a9e99afc6be4724ac51dc37a7e7644b'];

            $request_api = new GuzzleClient('GET', 'https://api.connected2fiber.com/availability/v3/companies/535/locations?limit=10&lat='.$lat.'&lon='.$long.'&radius='.$radious,$headers);
            $response = $client->send($request_api);
            
            $result_form = json_decode($response->getBody()->getContents(),true);
            //dd($result_form);
            $bandwidth = 'ETHERNET '.$request->bandwidth.'M';
            //dd("DIA".$dia_active);
            Session::forget('message');  
            if (count($result_form['body'][0]['locations'])) {
                $key = $result_form['body'][0]['locations'][0]['building_id'];
                $client = new Client(['base_uri' => 'https://api.connected2fiber.com']);
                $headers = ['Ocp-Apim-Subscription-Key' => '8a9e99afc6be4724ac51dc37a7e7644b'];
                $bandwidth = 'ETHERNET '.$request->bandwidth.'M';
                $request_api = new GuzzleClient('GET', 'https://api.connected2fiber.com/availability/v3/companies/535/locations/'.$key.'/products',$headers);
                $response = $client->send($request_api);
                $result = json_decode($response->getBody()->getContents(),true);
                //dd($result);
                //dd('https://api.connected2fiber.com/availability/v3/companies/535/locations/'.$key.'/products');
                $mrc = 0;
                $nrc = 0;
                if (isset($result['body'][0]['locations'][0]['products'])) {
                    //dd($result['body'][0]['locations'][0]['products']);
                    foreach ($result['body'][0]['locations'][0]['products'] as $registro) {
                        //dd($registro);
                        if ((trim($registro['transmission_rate']) == $bandwidth) AND ($registro['service_name'] == $service)) {
                            //dd($registro['speedName']);

                                $mrc_12 = $registro['mrc_12m'];
                                $nrc_12 = $registro['nrc_12m'];
                            
                                $mrc_24 = $registro['mrc_24m'];
                                $nrc_24 = $registro['nrc_24m'];
                            
                                $mrc_36 = $registro['mrc_36m'];
                                $nrc_36 = 0;

                                break;
                        }
                    }
                    //dd('hola');
                    $search = new SearchHistory;

                    $search -> address = $coord;
                    $search -> country = end($data->results)->formatted_address;
                    //dd($result['body'][0]['locations'][0]['address_information']['country']);
                    $search -> term = $term;
                    $search -> bandwidth = $bandwidth;
                    $search -> product = $type;
                    $search -> customer_name = $customer_name;
                    $search -> notes = $notes;
                    $search -> building_name = $result['body'][0]['locations'][0]['building_name'];
                    $search -> building_id = $result['body'][0]['locations'][0]['building_id'];
                    $search -> user_id = $id;
                    if ($term == 12) {
                        $search -> mrc = $mrc_12;
                        $search -> nrc = $nrc_12;
                    } elseif ($term == 24) {
                        $search -> mrc = $mrc_24;
                        $search -> nrc = $nrc_24;
                    } else {
                        $search -> mrc = $mrc_36;
                        $search -> nrc = 0;
                    }
                    $search -> save();
                } else {
                    $mrc_12 = 0;
                    $mrc_24 = 0;
                    $mrc_36 = 0;
                    $nrc_12 = 0;
                    $nrc_24 = 0;
                    $nrc_36 = 0;
                }
                //dd('voy');
                return view('index_lat', compact('result','company','mrc_12','nrc_12','mrc_24','nrc_24','mrc_36','nrc_36','term','bandwidth','customer_name','notes','dia_active','eth_active','type','key','lat','long'));
            } else {
                Session::flash('message', 'No results for this search! Please search for a different address'); 
                Session::flash('alert-class', 'alert-danger'); 
                return view('index_lat', compact('result_form','company','mrc','nrc','term','bandwidth','customer_name','notes','dia_active','eth_active','type','address','lat','long'));
            }
        }
        catch (\Exception $e) {
            dd($e);
            if($e instanceof \GuzzleHttp\Exception\ClientException ){
                Session::flash('message', 'No results for this search!'); 
                Session::flash('alert-class', 'alert-danger'); 
                if ($city <> '') {
                    $address .= ',+'.$city;
                }
                if ($country <> '') {
                    $address .= ',+'.$country;
                }
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                //dd($response);
                return view('index', compact('result_form','company','mrc','nrc','term','bandwidth','customer_name','notes','dia_active','eth_active','type','address'));
            } else {
                Session::flash('message', 'Problems connecting the server'); 
                Session::flash('alert-class', 'alert-danger');
                return view('index_lat', compact('result_form','company','mrc','nrc','term','bandwidth','customer_name','notes','dia_active','eth_active','type','address','lat','long'));
            }
          report($e);
          return false;
        }
    }
    public function search_key($type_search, Request $request)
    {
        try {
            $type = $request -> service_type;
            if ($request -> service_type == 'dia') {
                $dia_active = 'active';
                $eth_active = '';
                $service = 'Dedicated Internet';
            } elseif ($request -> service_type == 'ethernet') {
                $dia_active = '';
                $eth_active = 'active';
                $service = 'Ethernet - Switched';
            }
            $key = $request -> key;
            //$key = 'OZ82-C2F-670a22a8';
            $term = $request->term;
            $customer_name = $request->customer_name;
            $notes = $request->notes;
            //dd($request);
            $id = Auth::id();
            $userCompany = UserCompany::find($id);
            //dd($userCompany->id);
            $company = Companies::find($userCompany->company_id);
            //dd($company->title);
            $client = new Client(['base_uri' => 'https://api.connected2fiber.com']);
            //dd($client);
            $headers = ['Ocp-Apim-Subscription-Key' => '8a9e99afc6be4724ac51dc37a7e7644b'];
            //$request = new GuzzleClient('GET', 'https://api.connected2fiber.com/v2/buildings/parsed_address_search?companyId=535&street_name=Calle 4&page=0&size=100',$headers);
            if (isset($key)) {
                //dd($key);
                $bandwidth = $request->bandwidth;
                //dd($bandwidth);
                $request_api = new GuzzleClient('GET', 'https://api.connected2fiber.com/availability/v3/companies/535/locations/'.$key.'/products',$headers);
                //dd($request_api);
                $response = $client->send($request_api);
                $result = json_decode($response->getBody()->getContents(),true);
                //dd($result);
                $mrc = 0;
                $nrc = 0;
                if (isset($result['body'][0]['locations'][0]['products'])) {
                    foreach ($result['body'][0]['locations'][0]['products'] as $registro) {
                        if (($registro['transmission_rate'] == $bandwidth) AND ($registro['service_name'] == $service)) {
                            //dd($registro['speedName']);
                                $mrc_12 = $registro['mrc_12m'];
                                $nrc_12 = $registro['nrc_12m'];
                            
                                $mrc_24 = $registro['mrc_24m'];
                                $nrc_24 = $registro['nrc_24m'];
                            
                                $mrc_36 = $registro['mrc_36m'];
                                $nrc_36 = 0;
                        }
                    }
                    $search = new SearchHistory;

                    $search -> address = $result['body'][0]['locations'][0]['address_information']['street_address'];
                    
                    $search -> city = $result['body'][0]['locations'][0]['address_information']['city'];
                    $search -> state = $result['body'][0]['locations'][0]['address_information']['state'];
                    $search -> country = $result['body'][0]['locations'][0]['address_information']['country'];
                    //dd($result['body'][0]['locations'][0]['address_information']['country']);
                    $search -> term = $term;
                    $search -> bandwidth = $bandwidth;
                    $search -> product = $type;
                    $search -> customer_name = $customer_name;
                    $search -> notes = $notes;
                    $search -> building_name = $result['body'][0]['locations'][0]['building_name'];
                    $search -> building_id = $result['body'][0]['locations'][0]['building_id'];
                    $search -> user_id = $id;
                    if ($term == 12) {
                        $search -> mrc = $mrc_12;
                        $search -> nrc = $nrc_12;
                    } elseif ($term == 24) {
                        $search -> mrc = $mrc_24;
                        $search -> nrc = $nrc_24;
                    } else {
                        $search -> mrc = $mrc_36;
                        $search -> nrc = 0;
                    }
                    $search -> save();
                } else {
                    $mrc_12 = 0;
                    $mrc_24 = 0;
                    $mrc_36 = 0;
                    $nrc_12 = 0;
                    $nrc_24 = 0;
                    $nrc_36 = 0;
                }
            }
            
            //dd($result);
            //dd($result['serviceSpeedList']);
            //$bandwidth = 'ETHERNET '.$request->bandwidth.'M';
            if ($type_search == 'add') {
                return view('index', compact('result','company','mrc_12','nrc_12','mrc_24','nrc_24','mrc_36','nrc_36','term','bandwidth','customer_name','notes','dia_active','eth_active','type','key'));
            } elseif ($type_search == 'lat') {
                return view('index_lat', compact('result','company','mrc_12','nrc_12','mrc_24','nrc_24','mrc_36','nrc_36','term','bandwidth','customer_name','notes','dia_active','eth_active','type','key'));
            }
        }
        catch (\Exception $e) {
            if($e instanceof \GuzzleHttp\Exception\ClientException ){
                
                $response = $e->getResponse();
                //dd($response);
                $responseBodyAsString = $response->getBody()->getContents();
                return view('index', compact('result','company','mrc_12','nrc_12','mrc_24','nrc_24','mrc_36','nrc_36','term','bandwidth','customer_name','notes','dia_active','eth_active','type','key'));
            }
          report($e);
          return false;
        }
    }

    public function generate_pdf(Request $request) {
        //dd($request);
        if ($request -> product == 'dia') {
                $dia_active = 'active';
                $eth_active = '';
                $service = 'Dedicated Internet';
            } elseif ($request -> product == 'ethernet') {
                $dia_active = '';
                $eth_active = 'active';
                $service = 'Ethernet - Switched';
            }
        $id = Auth::id();
        $userCompany = UserCompany::find($id);
        $company = Companies::find($userCompany->company_id);
        $key = $request -> key;
        $coord = $request -> coord;
        $id = Auth::user();
        $bandwidth = $request->bandwidth;
        $headers = ['Ocp-Apim-Subscription-Key' => '8a9e99afc6be4724ac51dc37a7e7644b'];
        $client = new Client(['base_uri' => 'https://api.connected2fiber.com']);
        $request_api = new GuzzleClient('GET', 'https://api.connected2fiber.com/availability/v3/companies/535/locations/'.$key.'/products',$headers);
        $response = $client->send($request_api);
        $result = json_decode($response->getBody()->getContents(),true);
        //dd($result);
        if (isset($result['body'][0]['locations'][0]['products'])) {
                    foreach ($result['body'][0]['locations'][0]['products'] as $registro) {
                        //dd($registro['transmission_rate'].$bandwidth);
                        if (($registro['transmission_rate'] == $bandwidth) AND ($registro['service_name'] == $service)) {
                                //dd($registro['transmission_rate']);
                                $mrc_12 = $registro['mrc_12m'];
                                $nrc_12 = $registro['nrc_12m'];
                            
                                $mrc_24 = $registro['mrc_24m'];
                                $nrc_24 = $registro['nrc_24m'];
                            
                                $mrc_36 = $registro['mrc_36m'];
                                $nrc_36 = 0;
                        }
                    }
                }
        //dd($result);
        //dd($result['body'][0]['locations'][0]['products'][0]['mrc_12m']);
        $pdf = PDF::loadView('pdf.coord', compact('key','coord','request','id','mrc_12','nrc_12','mrc_24','nrc_24','mrc_36','nrc_36','result','company'));
            return $pdf->stream('document.pdf');
    }

    public function quote($id) {
        $record = SearchHistory::find($id);
        $key = $record -> building_id;
        $type = $record -> product;
        $bandwidth = $record -> bandwidth;
        $notes = $record -> notes;
        $customer_name = $record -> customer_name;
        $term = $record -> term;
        if ($type == 'dia') {
            $dia_active = 'active';
            $eth_active = '';
            $service = 'Dedicated Internet';
        } elseif ($type == 'ethernet') {
            $dia_active = '';
            $eth_active = 'active';
            $service = 'Ethernet - Switched';
        }
        $id = Auth::id();
        $userCompany = UserCompany::find($id);
        $company = Companies::find($userCompany->company_id);
        $headers = ['Ocp-Apim-Subscription-Key' => '8a9e99afc6be4724ac51dc37a7e7644b'];
        $client = new Client(['base_uri' => 'https://api.connected2fiber.com']);
        $request_api = new GuzzleClient('GET', 'https://api.connected2fiber.com/availability/v3/companies/535/locations/'.$key.'/products',$headers);
        $response = $client->send($request_api);
        $result = json_decode($response->getBody()->getContents(),true);
        //dd($result);
        if (isset($result['body'][0]['locations'][0]['products'])) {
                    foreach ($result['body'][0]['locations'][0]['products'] as $registro) {
                        //dd($registro['transmission_rate'].$bandwidth);
                        if ($registro['transmission_rate'] == $record -> bandwidth) {
                                //dd($registro['transmission_rate']);
                                $mrc_12 = $registro['mrc_12m'];
                                $nrc_12 = $registro['nrc_12m'];
                            
                                $mrc_24 = $registro['mrc_24m'];
                                $nrc_24 = $registro['nrc_24m'];
                            
                                $mrc_36 = $registro['mrc_36m'];
                                $nrc_36 = 0;
                        }
                    }
                }
        //dd($result);
        //dd($result['body'][0]['locations'][0]['products'][0]['mrc_12m']);
        return view('index', compact('result','company','mrc_12','nrc_12','mrc_24','nrc_24','mrc_36','nrc_36','term','bandwidth','customer_name','notes','dia_active','eth_active','type','key'));
    }

    public function history() {
        //$history = SearchHistory::where('user_id',Auth::user()->id)->orderBy('id')->get();
        //$history = SearchHistory::where ('id','>',272)->get();
        $history = SearchHistory::where('user_id','<>',2)->orderBy('id','desc')->get();
        //dd($history);
        /*
        foreach ($history as $rec) {
            $key = $rec -> building_id;
            //$rec -> id == 344 ? dd($rec -> building_id) : 1;
            $rec -> product == 'dia' ? $service = 'Dedicated Internet' : $service = 'Ethernet - Switched';
            //if ($key == 'OZ82-C2F-742f7a33') {
                //dd($rec -> id);
                $headers = ['Ocp-Apim-Subscription-Key' => '8a9e99afc6be4724ac51dc37a7e7644b'];
                $client = new Client(['base_uri' => 'https://api.connected2fiber.com']);
                $request_api = new GuzzleClient('GET', 'https://api.connected2fiber.com/availability/v3/companies/535/locations/'.$key.'/products',$headers);
                $response = $client->send($request_api);
                $result = json_decode($response->getBody()->getContents(),true);
                //dd($result);
            //}
            //$key == 'OZ82-C2F-742f7a33' ? dd($result) : 1;
            if (isset($result['body'][0]['locations'][0]['products'])) {
                    //dd($result['body'][0]['locations'][0]['products']);
                    foreach ($result['body'][0]['locations'][0]['products'] as $registro) {
                        //dd($registro['transmission_rate']);
                        if ($registro['transmission_rate'] == $rec -> bandwidth and $registro['service_name'] == $service) {
                            //dd($rec->id);
                            $mrc = 'mrc_'.$rec->term.'m';
                            $nrc = 'nrc_'.$rec->term.'m';
                            $mrc = $registro[$mrc];
                            $rec->term == 36 ? $nrc = 0 : $nrc = $registro[$nrc];
                            $rec -> mrc = $mrc;
                            $rec -> nrc = $nrc;
                            $rec -> building_name = $result['body'][0]['locations'][0]['building_name'];
                            $rec -> save();
                            //dd($mrc.' - '.$nrc);
                        }
                    }
                }
            //dd($result);
        } */
        return view('search_history', compact('history'));
    }
}
